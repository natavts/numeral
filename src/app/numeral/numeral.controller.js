'use strict';
var num2words = new NumberToWords();

angular.module('gulpAngularApp')

  .filter('numToMoney', function () {
    return function (input) {
      var _ref;

      if (input && typeof parseInt(input) === "number") {
        if (input.indexOf(".") !== -1) {
          var arr = input.split(".");
          _ref = num2words.convert(arr[0]) + ' dollars ' + num2words.convert(arr[1]) + ' cents';
        } else {
          _ref = num2words.convert(input) + ' dollars';
        }
        return _ref.charAt(0).toUpperCase() + _ref.slice(1);
      }
    }
  })
  .filter('numToWords', function () {
    return function (input) {
      var _ref;

      if (input && typeof parseInt(input) === "number") {
        if (input.indexOf(".") !== -1) {
          //console.log('point');
          var arr = input.split(".");
          var arr_after_point = arr[1].split(""),
            afterPoint = " ";
          arr_after_point.forEach(function (num) {
            afterPoint += num2words.convert(num) + ' '
          });
          _ref = num2words.convert(arr[0]) + ' point' + afterPoint;
        } else {
          //console.log('not point');
          //console.log(input);
          _ref = num2words.convert(input);
        }
        return _ref.charAt(0).toUpperCase() + _ref.slice(1);
      }
    }
  })
  .filter('numToYears', function () {
    return function (input) {
      var _ref;

      if (input && typeof parseInt(input) === "number") {
        if (input.indexOf(".") !== -1) {
          //console.log('point');
          var arr = input.split(".");
          var arr_after_point = arr[1].split(""),
            afterPoint = " ";
          arr_after_point.forEach(function (num) {
            afterPoint += num2words.convert(num) + ' '
          });
          _ref = num2words.convert(arr[0]) + ' point' + afterPoint;
        } else {
          //console.log('not point');
          //console.log(input);
          _ref = num2words.convert(input);
        }
        return _ref.charAt(0).toUpperCase() + _ref.slice(1);
      }
    }
  })
  .directive("scroll", function ($window) {
    return function (scope, element, attrs) {
      angular.element($window).bind("scroll", function () {
        if (this.pageYOffset >= 100) {
          scope.boolChangeClass = true;
          //console.log('Scrolled below header.');
        } else {
          scope.boolChangeClass = false;
          //console.log('Header is in view.');
        }
        scope.$apply();
      });
    };
  })
  .controller('NumeralCtrl', function TodoCtrl($scope, $location, Numeral, $interval) {
    $scope.data = {
      input: ''
    };
    if (Numeral.input) {
      $scope.data.input = Numeral.input;
    }

    $scope.styleVar = {
      cursor: 'off'
    };
    $scope.$watch(function (scope) {
        return scope.data.input
      },
      function (newValue) {
        Numeral.input = newValue;
      }
    );


    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };


    $interval(callAtInterval, 500);

    function callAtInterval() {
      $scope.styleVar.cursor = $scope.styleVar.cursor ? '' : 'off';
    }

    $scope.button = function (val) {
      //console.log($scope.data.input);
      if (val === 'delete') {
        $scope.data.input = $scope.data.input.substring(0, $scope.data.input.length - 1)
      }
      if ($scope.data.input.length < 16 && val !== 'delete') {

        $scope.data.input += val;
      }
    }

  });
