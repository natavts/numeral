'use strict';

angular.module('gulpAngularApp', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ngRoute', 'ui.bootstrap', 'puElasticInput'])
  .config(function ($routeProvider) {
    $routeProvider
      //.when('/', {
      //  templateUrl: 'app/main/main.html',
      //  controller: 'MainCtrl'
      //})
      .when('/todo', {
        templateUrl: 'app/todo/todo.html',
        controller: 'TodoCtrl'
      })
      .when('/numeral/money', {
        templateUrl: 'app/numeral/numeral.html',
        controller: 'NumeralCtrl'
      })
      .when('/numeral/words', {
        templateUrl: 'app/numeral/numeral.html',
        controller: 'NumeralCtrl'
      })
      .otherwise({
        redirectTo: '/numeral/money'
      });
  })
  //.factory('todoStorage', function () {
  //  var STORAGE_ID = 'todos-angularjs-perf';
  //
  //  return {
  //    get: function () {
  //      return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
  //    },
  //
  //    put: function (todos) {
  //      //alert("saving:"+JSON.stringify(todos));
  //      localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
  //    }
  //  };
  //})
  //.directive('todoEscape', function () {
  //  var ESCAPE_KEY = 27;
  //  return function (scope, elem, attrs) {
  //    elem.bind('keydown', function (event) {
  //      if (event.keyCode === ESCAPE_KEY) {
  //        scope.$apply(attrs.todoEscape);
  //      }
  //    });
  //  };
  //})
  //.directive('todoFocus', function ($timeout) {
  //  return function (scope, elem, attrs) {
  //    scope.$watch(attrs.todoFocus, function (newVal) {
  //      if (newVal) {
  //        $timeout(function () {
  //          elem[0].focus();
  //        }, 0, false);
  //      }
  //    });
  //  };
  //})
;
