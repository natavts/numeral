this.NumberToWords = (function() {
  var ones, scales, tens;

  function NumberToWords() {}

  ones = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];

  tens = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];

  scales = ["", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion"];

  NumberToWords.prototype.convert = function(num) {
    var n, number, numberPart, previousScale, previousScaleValue, remainder, scaleValue, word, _i, _ref;
    number = Math.abs(num);
    if (number < 100) {
      return this.check_negative(num, this.convert_less_than_hundred(number));
    }
    if (number < 1000) {
      return this.check_negative(num, this.convert_less_than_thousand(number));
    }
    for (n = _i = 0, _ref = scales.length; 0 <= _ref ? _i <= _ref : _i >= _ref; n = 0 <= _ref ? ++_i : --_i) {
      previousScale = n - 1;
      scaleValue = Math.pow(1000, n);
      if (scaleValue > number) {
        previousScaleValue = Math.pow(1000, previousScale);
        numberPart = parseInt(number / previousScaleValue, 10);
        remainder = number - (numberPart * previousScaleValue);
        word = this.convert_less_than_thousand(numberPart) + " " + scales[previousScale];
        if (remainder > 0) {
          word = word + " " + this.convert(remainder); //,
        }
        return this.check_negative(num, word);
      }
    }
  };

  NumberToWords.prototype.convert_less_than_hundred = function(number) {
    var i, tempVal, _i, _ref;
    if (number < 20) {
      return ones[number];
    }
    for (i = _i = 0, _ref = tens.length; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
      tempVal = 20 + 10 * i;
      if (tempVal + 10 > number) {
        if (number % 10 !== 0) {
          return tens[i] + "-" + ones[number % 10];
        }
        return tens[i];
      }
    }
  };

  NumberToWords.prototype.convert_less_than_thousand = function(number) {
    var modulus, remainder, word;
    word = "";
    remainder = parseInt(number / 100, 10);
    modulus = parseInt(number % 100, 10);
    if (remainder > 0) {
      word = ones[remainder] + " hundred";
      if (modulus > 0) {
        word = word + " ";
      }
    }
    if (modulus > 0) {
      word = word + this.convert_less_than_hundred(modulus);
    }
    return word;
  };

  NumberToWords.prototype.check_negative = function(number, word) {
    if (number < 0) {
      return "Negative " + word;
    } else {
      return word;
    }
  };

  return NumberToWords;

})();
